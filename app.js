const express = require("express");
const {users, conversations} = require("./models");

const app = express();

//findAll -> select * from table
//create -> insert into table columns values
//update -> update table set
//destroy -> delete from table

app.get('/users', async(req, res) => {
  const results = await users.findAll();
  res.json(results);
});

app.get('/conversations', async(req, res) => {
  const results = await conversations.findAll({include: [{model: users, as: "created_by_user"}]});
  res.json(results);
});

module.exports = app;